# timothy
The code for Cougar Crazies's first FIRST robot, for FIRST 2023 "Charged Up". 
The robot's name is Timothy. It is powered by NI's roboRIO 2.0.

> I have no idea what I'm doing and I've never worked with Java  
> \- Andy "12beesinatrenchcoat" Chan, writing this README
l
# Controls
Curvature drive is used.
Driven at competition with a DualShock 4 controller.

- Left stick up/down for forwards/backwards movement.
- Right stick left/right for arc left/right.
- L2 to reduce maximum speed.
- R2 to increase maximum speed.

# Building
Use WPILib VS Code (2023 was used, your experience may vary on other versions).

Victor SPX motor controllers were used, with CAN Bus IDs 1 and 2 assigned to the left drive, and 3 and 4 assigned to the right drive.

1. Connect the computer to the robot (use ethernet for more reliability or if you're at competition!)
2. Open the Command Palette (`Ctrl` + `Shift` + `P`), Select `WPILib: Build Robot Code`.
3. Open the Command Palette again! Select `WPILib: Deploy Robot Code`.
    - There's also the `Shift` + `F5` shortcut. We didn't use it, but you might like it.

Best of luck, future teams! You'll do amazing! <3

# License
WPILib and its example code (which is 99% of this) is under the BSD license (see [WPILib-License.md](./WPILib-License.md)).

Any changes we made are under the Unlicense (basically public domain, see [LICENSE.txt](./LICENSE.txt)). Basically only [Robot.java](./src/main/java/frc/robot/Robot.java) was modified, though.

That's about it.
