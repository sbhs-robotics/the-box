// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

// import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.TimedRobot; // Extended, ignore warning


/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends TimedRobot {
	private final MotorControllerGroup m_leftDrive = new MotorControllerGroup(
		new WPI_VictorSPX(1), new WPI_VictorSPX(2)
	);
	private final MotorControllerGroup m_rightDrive = new MotorControllerGroup(
		new WPI_VictorSPX(3), new WPI_VictorSPX(4)
	);

	// Drive
	private final DifferentialDrive m_robotDrive = new DifferentialDrive(m_leftDrive, m_rightDrive);
	private final Joystick m_driveController = new Joystick(0); // Driving
	private final Timer m_timer = new Timer();

	public double maxSpeed = 0.50;

	/**
	 * This function is run when the robot is first started up and should be used for any
	 * initialization code.
	 */
	@Override
	public void robotInit() {
		// We need to invert one side of the drivetrain so that positive voltages
		// result in both sides moving forward. Depending on how your robot's
		// gearbox is constructed, you might have to invert the left side instead.
		m_rightDrive.setInverted(true);
		CameraServer.startAutomaticCapture();
	}

	/** This function is run once each time the robot enters autonomous mode. */
	@Override
	public void autonomousInit() {
		m_timer.restart();
	}

	/** This function is called periodically during autonomous. */
	@Override
	public void autonomousPeriodic() {
		// Drive for 1 second
		if (m_timer.get() < 1.0) {
			// Drive forwards half speed, make sure to turn input squaring off
			m_robotDrive.tankDrive(0.25, 0.25, false);
		} else if (m_timer.get() < 2.0) {
			m_robotDrive.tankDrive(0, 0);
		} else if (m_timer.get() < 3.0) {
			m_robotDrive.tankDrive(-0.5, -0.48, false);
		} else if (m_timer.get() < 7.0) {
			m_robotDrive.stopMotor(); // stop robot
		}
	}

	/** This function is called once each time the robot enters teleoperated mode. */
	@Override
	public void teleopInit() {
		maxSpeed = 0.50;
	}

	/** This function is called periodically during teleoperated mode. */
	@Override
	public void teleopPeriodic() {
		/* Driving  */
		// L2 should reduce speed, R2 should increase speed
		maxSpeed = 
			(0.50 + (Math.abs(m_driveController.getRawAxis(2) / 2))) // R2
			* (1 - Math.abs(m_driveController.getRawAxis(3) * 0.3)); // L2
		
		m_robotDrive.curvatureDrive(
			-m_driveController.getRawAxis(1) * maxSpeed, 
			-m_driveController.getRawAxis(4) * maxSpeed, 
			true);
	}

	/** This function is called once each time the robot enters test mode. */
	@Override
	public void testInit() {}

	/** This function is called periodically during test mode. */
	@Override
	public void testPeriodic() {}
}
